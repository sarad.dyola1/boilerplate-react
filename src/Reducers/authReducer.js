import { USER_LOGIN } from "../Helpers/constants";

const initState = {
    isAuth: '',
    user: {},
    token: '',
    redirectToReferrer: false
}

const authReducer = (state=initState, action) => {
    let {type, payload} = action
    // let {isAuth, user, token, redirectToReferrer} = payload
    switch(type){
        case USER_LOGIN:
            return {
                ...state,
                ...payload
            }
        default:
            return state;
    }
}

export default authReducer
import axios from "axios";

let auth = {
    userLogin: body => axios.post('/users/login', body)
}

export default {
    ...auth
}
import Home from '../Views/Pages/Home';

const routes = [
	{
        path:'/',
        component: Home,
        exact: true
    }
]

export default routes
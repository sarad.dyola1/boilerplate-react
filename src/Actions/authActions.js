import Cookies from "universal-cookie";
import request from "../Helpers/request";
import { USER_LOGIN } from "../Helpers/constants";

const cookies = new Cookies();

export const userLogin = body => dispatch => {
  return request.userLogin(body).then(data => {
    cookies.set("token", data.data.data.access_token, {
      maxAge: 60 * 60 * 24 * 7
    });
    cookies.set("user", JSON.stringify(data.data.user), {
      maxAge: 60 * 60 * 24 * 7
    });
    cookies.set("isAuth", true, { maxAge: 60 * 60 * 24 * 7 });
    let payload = {
      isAuth: "true",
      token: data.data.data.access_token,
      user: data.data.user,
      redirectToReferrer: true
    };

    dispatch({ type: USER_LOGIN, payload });
  });
};

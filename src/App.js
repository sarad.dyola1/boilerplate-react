import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import routes from "./Helpers/routes";

const App = (props) => {
  let { auth } = props;
  const PrivateRoute = ({ component: Component, ...rest }) => {
    return (
      <Route
        {...rest}
        render={(props) =>
          auth.isAuth === "true" ? (
            <Component {...props} />
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: props.location },
              }}
            />
          )
        }
      />
    );
  };
  
  return (
    <>
      <Switch>
        {routes.map((route, i) => {
          return route.protected === true ? (
            <PrivateRoute
              key={i}
              {...route}
              component={route.component}
            />
          ) : (
            <Route
              key={i}
              {...route}
              component={route.component}
            />
          );
        })}
      </Switch>
    </>
  );
};

export default App;
